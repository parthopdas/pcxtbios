;---------------------------------------------------------------------------------------------------
; Int 9h Enhanced Keyboard Support. Intercepts and processes escaped scan codes (E0 prefix) and
; "fake" shifts before the standard Int 9h handler.
;---------------------------------------------------------------------------------------------------
ifdef	ENHANCED_KEYB
proc	int_9_enhanced	near

	cmp	al, 0E0h			; Is scan code E0?
	jne	@@continue			; No

	or	bl, 00000010b			; Set flag to indicate last scan code was E0h
@@ignore:
	pop	cx
	;;;;++++push	OFFSET int_9_end		; Skip the standard Int 9h
	jmp	short @@done

@@continue:
	cmp	al, 57h				; F11 pressed?
	je	@@F11_F12
	cmp	al, 58h				; F12 pressed?
	je	@@F11_F12

	test	bl, 00000010b			; Was last scan code E0h?
	jz	@@done				; No

	and	bl, 11111101b			; Clear flag to indicate last scan code wasn't E0h

	cmp	al, 0AAh			; Check for escaped key up code
	jne	@@not_ext_up
	mov	al, bh				; If it is, get last escaped key down code
	or	al, 10000000b			; Set high bit to convert to key up
	mov	ah, al				; Save copy
@@not_ext_up:
	mov	bh, al				; Save current escaped scan code

	mov	cl, al				; Ignore all fake shifts
	and	cl, 01111111b
	cmp	cl, 02Ah
	je	@@ignore
	cmp	cl, 036h
	je	@@ignore

	cmp	al, 35h				; Process / with standard Int 9h
	je	@@done
	cmp	al, 1Ch				; ENTER
	je	@@done
	cmp	al, 1Dh				; Right CTRL
	je	@@done
	cmp	al, 38h				; Right ALT
	je	@@done
	cmp	al, 46h				; CTRL+Pause (Break)
	je	@@done

	call	check_ctrl_alt_del		; Check for CTRL+ALT+DEL combination with gray DEL

	call	set_insert_flags		; Check for gray INS key down and set insert flags
	jc	@@done				; Process key up codes normally

	mov	al, 0E0h			; Set ASCII to E0h for extended keystroke

@@stuff:
	pop	cx
	;;;;++++push	offset int_9_stuff		; Skip processing and put keystroke in buffer

@@done:
	mov	[ds:96h], bx			; Write back key flags
	ret

@@F11_F12:
	add	ah, 2Eh				; Convert protocol scan code to software

	mov	al, [ds:17h]
	test	al, 00001000b			; ALT key pressed?
	jnz	@@alt				;   yes
	test	al, 00000100b			; CTRL key pressed?
	jnz	@@ctrl				;   yes
	test	al, 00000011b			; Either shift key pressed?
	jnz	@@shift				;   yes
	jmp	short @@stuff_F11_F12

@@alt:
	add	ah, 2				; Adjust scan code for ALT+F11/F12
@@ctrl:
	add	ah, 2				; Adjust scan code for CTRL+F11/F12
@@shift:
	add	ah, 2				; Adjust scan code for SHIFT+F11/F12

@@stuff_F11_F12:
	xor	al, al				; Set ASCII to 0
	jmp	short @@stuff			; Stuff keystroke

endp	int_9_enhanced
endif


;---------------------------------------------------------------------------------------------------
; Interrupt 19h - Warm Boot
;---------------------------------------------------------------------------------------------------
	entry	0E6F2h				; IBM entry point for int 19h
proc	int_19	far

	jmp	ipl				; Warm boot

endp	int_19


;---------------------------------------------------------------------------------------------------
; Detect CPU type (8088 or V20)
;---------------------------------------------------------------------------------------------------
proc	cpu_check	near			; Test for 8088 or V20 CPU

	xor	al, al				; Clean out al to set ZF
	mov	al, 40h				; mul on V20 does not affect the zero flag
	mul	al				;   but on an 8088 the zero flag is used
	jz	@@have_v20			; Was zero flag set?
	mov	si, offset str_8088		;   No, so we have an 8088 CPU
	ret
@@have_v20:
	mov	si, offset str_v20		;   Otherwise we have a V20 CPU
	ret

endp	cpu_check


;---------------------------------------------------------------------------------------------------
; Detect if a FPU/NPU (8087) is present
;---------------------------------------------------------------------------------------------------
proc	fpu_check	near			; Test for FPU

	fninit					; Try to init FPU
	mov	si, 0200h
	mov	[byte si+1], 0			; Clear memory byte
	fnstcw	[word si]			; Put control word in memory
	mov	ah, [si+1]
	cmp	ah, 03h				; If ah is 03h, FPU is present
	jne	@@no_8087
	or	[byte ds:10h], 00000010b	; Set FPU in equp list
	ret
@@no_8087:
	and	[byte ds:10h], 11111101b	; Set no FPU in equp list
	ret

endp	fpu_check


;---------------------------------------------------------------------------------------------------
; Interrupt 14h - Serial RS232 Communications
;---------------------------------------------------------------------------------------------------
	entry	0E729h				; IBM entry point for baud rate generator table

baud	dw	0417h				;  110 baud clock divisor
	dw	0300h				;  150 baud clock divisor
	dw	0180h				;  300 baud clock divisor
	dw	00C0h				;  600 baud clock divisor
	dw	0060h				; 1200 baud clock divisor
	dw	0030h				; 2400 baud clock divisor
	dw	0018h				; 4800 baud clock divisor
	dw	000Ch				; 9600 baud clock divisor

	entry	0E739h				; IBM entry point for int 14h
proc	int_14	far

	sti					; Serial RS232 COM services
	push	ds				;   through 8250 UART (ugh)
	push	dx				;   dx = COM device (0 - 3)
	push	si
	push	di
	push	cx
	push	bx
	mov	bx, 40h
	mov	ds, bx
	mov	di, dx				;
	mov	bx, dx				; RS232 serial COM index (0-3)
	shl	bx, 1				;   index by bytes
	mov	dx, [bx]			; Convert index to port number
	or	dx, dx				;   by indexing 40:0
	jz	@@end				;   no such COM device, exit
	or	ah, ah				; Init on ah=0
	jz	@@init
	dec	ah
	jz	@@send				; Send on ah=1
	dec	ah
	jz	@@receive			; Receive on ah=2
	dec	ah
	jz	@@status			; Status on ah=3

@@end:
	pop	bx				; End of COM service
	pop	cx
	pop	di
	pop	si
	pop	dx
	pop	ds
	iret

@@init:
	push	ax				; Init COM port, al has data
						; = (Word Length in Bits - 5)
						;  +(1 if two stop bits) *  4
						;  +(1 if parity enable) *  8
						;  +(1 if parity even  ) * 16
						;  +(BAUD: select 0-7  ) * 32
	mov	bl, al
	add	dx, 3				; Line Control Register (LCR)
	mov	al, 80h 			;   index RS232_BASE + 3
	out	dx, al				; Tell LCR to set (latch) baud
	mov	cl, 4
	rol	bl, cl				; Baud rate selects by words
	and	bx, 00001110b			;   mask off extraneous
	mov	ax, [word cs:bx+baud]		; Clock divisor in ax
	sub	dx, 3				; Load in lo order baud rate
	out	dx, al				;   index RS232_BASE + 0
	inc	dx				; Load in hi order baud rate
	mov	al, ah
	out	dx, al				;   index RS232_BASE + 1
	pop	ax
	inc	dx				; Find Line Control Register
	inc	dx				;   index RS232_BASE + 3
	and	al, 00011111b			; Mask out the baud rate
	out	dx, al				;   set (censored) init stat
	mov	al, 0
	dec	dx				; Interrupt Enable Reg. (IER)
	dec	dx				;   index RS232_BASE + 1
	out	dx, al				; Interrupt is disabled
	dec	dx
	jmp	short @@status			; Return current status

@@send:
	push	ax				; Send al through COM port
	mov	al, 3
	mov	bh, 00110000b			; (Data Set Ready, Clear To Send)
	mov	bl, 00100000b			;   (Data Terminal Ready) wait
	call	@@wait				; Wait for transmitter to idle
	jnz	@@timeout			;   time-out error
	sub	dx, 5				;   (xmit) index RS232_BASE
	pop	cx				; Restore char to cl register
	mov	al, cl				;   get copy to load in UART
	out	dx, al				;   transmit char to 8250
	jmp	@@end				;   ah register has status

@@timeout:
	pop	cx				; Transmit error, restore char
	mov	al, cl				;   in al for compatibility
						;   fall through to generate error
@@timeout_2:
	or	ah, 80h 			; Set error (=sign) bit in ah
	jmp	@@end				;   common exit

@@receive:
	mov	al, 1				; Get char. from COM port
	mov	bh, 00100000b			; Wait on DSR (Data Set Ready)
	mov	bl, 00000001b			; Wait on DTR (Data Terminal Ready)
	call	@@wait				;   wait for character
	jnz	@@timeout_2			;   time-out error
	and	ah, 00011110b			; Mask ah for error bits
	sub	dx, 5				;   (receiver) index RS232_BASE
	in	al, dx				; Read the character
	jmp	@@end				;   ah register has status

@@status:
	add	dx, 5				; Calculate line control stat
	in	al, dx				;   index RS232_BASE + 5
	mov	ah, al				;   save high order status
	inc	dx				; Calculate modem stat. reg.
	in	al, dx				;   index RS232_BASE + 6
	jmp	@@end				;   save low  order status
						; ax = (DEL Clear_To_Send ) *     1
						;      (DEL Data_Set_ready) *     2
						;      (Trailing_Ring_Det.) *     4
						;      (DEL Carrier_Detect) *     8
						;      (Clear_To_Send     ) *    16
						;      (Data_Set_Ready    ) *    32
						;      (Ring_Indicator    ) *    64
						;      (Carrier_Detect    ) *   128
						;      ----------------------------
						;      (Char  received    ) *   256
						;      (Char smothered    ) *   512
						;      (Parity error      ) *  1024
						;      (Framing error     ) *  2048
						;      (Break detected    ) *  4096
						;      (Able to xmit      ) *  8192
						;      (Transmit idle     ) * 16384
						;      (Time out error    ) * 32768
@@poll:
	mov	bl, [byte di+7Ch]		; Wait on bh in status or error

@@loop:
	sub	cx, cx				; Outer delay loop
@@loop_2:
	in	al, dx				;   inner loop
	mov	ah, al
	and	al, bh				; And status with user bh mask
	cmp	al, bh
	jz	@@loop_exit			;   jump if mask set
	loop	@@loop_2			; Else try again
	dec	bl
	jnz	@@loop
	or	bh, bh				; Clear mask to show timeout
@@loop_exit:
	retn					; Exit ah reg. Z flag status

@@wait: add	dx, 4				; Reset the Modem Control Reg.
	out	dx, al				;   index RS232_BASE + 4
	inc	dx				; Calculate Modem Status Reg.
	inc	dx				;   index RS232_BASE + 6
	push	bx				; Save masks (bh=MSR, bl=LSR)
	call	@@poll				;   wait on MSR modem status
	pop	bx				;   restore wait masks bh, bl
	jnz	@@done				;   "Error Somewhere" by dec

	dec	dx				; Calculate Line Status Reg.
	mov	bh, bl				;   index RS232_BASE + 5
	call	@@poll				;   wait on LSR line status
@@done:
	retn					; Status in ah reg and Z flag

endp	int_14
